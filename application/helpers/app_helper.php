<?php

if( ! function_exists( 'asset' ) ){
	function asset() 
	{
      return base_url().'assets/';
	}
}

if( ! function_exists( 'dateDiff' ) ){
	function distancia($lat1, $lon1, $lat2, $lon2) {

		$lat1 = deg2rad($lat1);
		$lat2 = deg2rad($lat2);
		$lon1 = deg2rad($lon1);
		$lon2 = deg2rad($lon2);

		$dist = (6371 * acos( cos( $lat1 ) * cos( $lat2 ) * cos( $lon2 - $lon1 ) + sin( $lat1 ) * sin($lat2) ) );
		$dist = number_format($dist, 2, '.', '');
		return $dist;
	}
}

if( ! function_exists( 'dateDiff' ) ){
	function dateDiff( $dateStart, $dateEnd, $format = '%H:%i:%s' ) {
	    $d1     =   new DateTime( $dateStart );

	    $d2     =   new DateTime( $dateEnd );

	    //Calcula a diferença entre as datas
	    $diff   =   $d1->diff($d2, true);   

	    //Formata no padrão esperado e retorna
	    return $diff->format( $format );
	}
}

if( ! function_exists( 'get_date' ) ){
	function get_date($timestamp)
	{
		$data = gmdate('Y-m-d',$timestamp/1000);

		return $data;
	}
}